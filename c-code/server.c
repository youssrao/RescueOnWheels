/*
    C socket server example, handles multiple clients using threads
    Compile
    gcc server.c -lpthread -o server
*/
 
#include<stdio.h>
#include<string.h>    //strlen
#include<stdlib.h>    //strlen
#include<sys/socket.h>
#include<arpa/inet.h> //inet_addr
#include<unistd.h>    //write
#include<pthread.h> //for threading , link with lpthread
 
//the thread function
void *connection_handler(void *);
 
int main(int argc , char *argv[])
{
    int socket_desc , client_sock , c;
    struct sockaddr_in server , client;
     
    //Create socket
    socket_desc = socket(AF_INET , SOCK_STREAM , 0);
    if (socket_desc == -1)
    {
        printf("Could not create socket");
    }
    puts("Socket created");
     
    //Prepare the sockaddr_in structure
    server.sin_family = AF_INET;
    server.sin_addr.s_addr = INADDR_ANY;
    server.sin_port = htons( 8889 );
     
    //Bind
    if( bind(socket_desc,(struct sockaddr *)&server , sizeof(server)) < 0)
    {
        //print the error message
        perror("bind failed. Error");
        return 1;
    }
    puts("bind done");
     
    //Listen
    listen(socket_desc , 3);
     
    //Accept and incoming connection
    puts("Waiting for incoming connections...");
    c = sizeof(struct sockaddr_in);
    
	pthread_t thread_id;
	
    while( (client_sock = accept(socket_desc, (struct sockaddr *)&client, (socklen_t*)&c)) )
    {
        puts("Connection accepted");
         
        if( pthread_create( &thread_id , NULL ,  connection_handler , (void*) &client_sock) < 0)
        {
            perror("could not create thread");
            return 1;
        }
         
        //Now join the thread , so that we dont terminate before the thread
        //pthread_join( thread_id , NULL);
        puts("Handler assigned");
    }
     
    if (client_sock < 0)
    {
        perror("accept failed");
        return 1;
    }
     
    return 0;
}
 
/*
 * This will handle connection for each client
 * */
void *connection_handler(void *socket_desc)
{
    //Get the socket descriptor
    int sock = *(int*)socket_desc;
    int read_size;
    int i;
    char client_message[2000];
    char joystickX[2000];
    char joystickY[2000];
    char * pch;
    float joystickMax = 50;
    float joystickXFloat, joystickYFloat;
    float joystickXPro, joystickYPro;
    float joystickXProAbs, joystickYProAbs;
    float v, w, r, l;
    
    //Send some messages to the client
//    message = "Greetings! I am your connection handler\n";
//    write(sock , message , strlen(message));
//
//    message = "Now type something and i shall repeat what you type \n";
//    write(sock , message , strlen(message));
    
    //Receive a message from client
    while( (read_size = recv(sock , client_message , 2000 , 0)) > 0 )
    {
        //end of string marker
		client_message[read_size] = '\0';
//        printf ("%s\n",client_message);

        
        pch = strtok (client_message," ");
        if (pch != NULL)
        {
            if (i == 0) {
                strcpy(joystickX, pch);
                i++;
            }
//            printf ("%s\n",pch);
            pch = strtok (NULL, " ");
            if (i == 1) {
                strcpy(joystickY, pch);
                i++;
            }
            
            i = 0;

        }

//        printf("x %s y %s\n", joystickX, joystickY);
        
        joystickXFloat = (float)atof(joystickX);
        joystickYFloat = (float)atof(joystickY);
        
        joystickXPro = ((joystickXFloat / joystickMax) * 100);
        joystickYPro = ((joystickYFloat / joystickMax) * 100);
        
        joystickXPro = joystickXPro * -1;
        
        if (joystickXPro < 0) {
            joystickXProAbs = joystickXPro * -1;
        }
        else {
            joystickXProAbs = joystickXPro;
        }
        
        if (joystickYPro < 0) {
            joystickYProAbs = joystickYPro;
        }
        else {
            joystickYProAbs = joystickYPro;
        }
        
        v = (100 - joystickXProAbs) * (joystickYPro/100) + joystickYPro;
        w = (100 - joystickYProAbs) * (joystickXPro/100) + joystickXPro;
        r = ((v+w)/2);
        l = ((v-w)/2);
        
        printf("x %3.6f y %3.6f\n", joystickXPro, joystickYPro);
        printf("v %3.6f w %3.6f\n", v, w);
        printf("l %3.6f r %3.6f\n", l, r);

        
        
        
//        printf(" %s\n", client_message);
		//Send the message back to client
//        write(sock , client_message , strlen(client_message));
		
		//clear the message buffer
		memset(client_message, 0, 2000);
    }
     
    if(read_size == 0)
    {
        puts("Client disconnected");
        fflush(stdout);
    }
    else if(read_size == -1)
    {
        perror("recv failed");
    }
         
    return 0;
} 
