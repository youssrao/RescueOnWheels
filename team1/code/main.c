#include <unistd.h>                             
#include <fcntl.h>                            
#include <sys/ioctl.h>                  
#include <linux/i2c-dev.h>              
#include <stdio.h>
#include <unistd.h>
#include <wiringPi.h>
#include <wiringPiI2C.h>
#include <pthread.h>
#include <softPwm.h>
#include <math.h>
#include<string.h>
#include<stdlib.h>
#include<sys/socket.h>
#include<arpa/inet.h>
#define I2C_FILE "/dev/i2c-1"
#define PI 3.14159265359

typedef unsigned char uint8_t;
char config[2], c;
char client_message[2000];
int power = 180;
int file, fd, ds;
int socket_desc, client_sock, d;
float direction;
struct sockaddr_in server, client;

uint8_t Totalpower[2] = { 4, power };
//set total power to 230
uint8_t Softstart[3] = { 0x91, 23, 0 };
//create softstart to prevent motor overloading
uint8_t TxData1[2] = { 00, 0x51 };
//add i2c adress for reading
uint8_t TxData2[1] = { 0x03 };
//add offset
uint8_t low = 50;
//data offset for low byte order
uint8_t MotorHF[7] = { 7, 3, 0xa5, 2, 3, 0xa5, 2 };
// High speed forward
uint8_t MotorST[7] = { 7, 0, 0, 0, 0, 0, 0 };
// Stop 
uint8_t MotorHR[7] = { 7, 3, 0xa5, 1, 3, 0xa5, 1 };
//High speed reverse
uint8_t MotorHFR[7] = { 7, 3, 0xa5, 2, 3, 0xa5, 1 };
//High speed forward right
uint8_t MotorHFL[7] = { 7, 3, 0xa5, 1, 3, 0xa5, 2 };
//High speed forward left

void *connection_handler(void *);

void motorInit() {
	wiringPiSetup();
	pullUpDnControl(0, PUD_DOWN);
	fd = wiringPiI2CSetup(0x32);
	write(fd, &Totalpower[0], 2);
	write(fd, &Softstart[0], 3);
	usleep(200);
	printf("Motor init succeeded. \n");
}
void compassInit() {
	if ((file = open(I2C_FILE, O_RDWR)) < 0) {
		printf("Failed to open the bus. \n");
	}
	// Get I2C device, HMC5883 I2C address is 0x1E(30)
	ioctl(file, I2C_SLAVE, 0x1E);
	// Select Configuration register A(0x00)
	config[0] = 0x00;
	// Normal measurement configuration, data rate o/p = 0.75 Hz(0x60)
	config[1] = 0x60;
	write(file, config, 2);
	// Select Mode register(0x02)
	config[0] = 0x02;
	// Continuous measurement mode(0x00)
	config[1] = 0x00;
	write(file, config, 2);
	usleep(150);
	printf("Compass init succeeded. \n");
}

void distanceInit() {
	ds = wiringPiI2CSetup(0x70);
	usleep(1000);
	printf("Distance init succeeded. \n");
}

void connectionInit() {
	direction = 0;
	socket_desc = socket(AF_INET, SOCK_STREAM, 0);
	if (socket_desc == -1) {
		printf("Could not create socket. \n");
	}
	printf("Socket created. \n");

	//Prepare the sockaddr_in structure
	server.sin_family = AF_INET;
	server.sin_addr.s_addr = INADDR_ANY;
	server.sin_port = htons(8888);

	//Bind
	if (bind(socket_desc, (struct sockaddr *) &server, sizeof(server)) < 0) {
		//print the error message
		perror("Bind failed. Error. \n");
	}
	printf("Bind done. \n");

	printf("Connection init succeeded. \n");

}

void* motor(void* param) {
	while (1) {
		if(direction > -0.5 && direction < 0.5){
			write(fd,&MotorHF[0],7);
		}
		else if(direction < -2.64159265359 && direction > 2.64159265359){
			write(fd,&MotorHR[0],7);
		}
		else if(direction < -0.5 && direction > -2.64159265359){
			write(fd,&MotorHFR[0],7);
		}
		else if(direction > 0.5 && direction < 2.64159265359){
			 write(fd,&MotorHFL[0],7);
		}
		else if (direction == 0 ){
			write(fd,&MotorST[0],7);
		}
		else{
			write(fd,&MotorST[0],7);
		}
	}
}

void* compass(void* param) {
	while (1) {
		char reg[1] = { 0x03 };
		write(file, reg, 1);
		char data[6] = { 0 };
		if (read(file, data, 6) != 6) {
			printf("Erorr : Input/output Erorr. \n");
		} else {
			// Convert the data
			int xAxis = (data[0] * 256 + data[1]);
			if (xAxis > 32767) {
				xAxis -= 65536;
			}

			int yAxis = (data[4] * 256 + data[5]);
			if (yAxis > 32767) {
				yAxis -= 65536;
			}

			double val;
			double grad = (yAxis / xAxis);
			double deg;

			val = (180.0 / PI);
			deg = (atan(grad) * val);
			printf("The angle is %lf degrees. \n", deg);
			usleep(100000);
		}
	}
}

void* distance(void* param) {
	while (1) {
		// start a new measurement in centimeters
		write(ds, &TxData1[0], 2);
		//give the sensor time for measurement
		usleep(50000);
		//ask for the lower order byte of the range
		write(ds, &TxData2[0], 1);
		read(ds, &low, 1);
		printf("Distance is %d \n", low);
		usleep(100000);
	}
	return 0;
}

void *connection_handler(void *socket_desc) {
	//Get the socket descriptor
	int sock = *(int*) socket_desc;
	int read_size;
	char *message;

	//Send some messages to the client
	message = "Greetings! I am your connection handler. \n";
	write(sock, message, strlen(message));

	message = "Now type something and i shall repeat what you type. \n";
	write(sock, message, strlen(message));
	
	
	//Receive a message from client
	while ((read_size = recv(sock, client_message, 5, 0)) > 0) {
		//end of string marker
		client_message[5] = '\0';
		printf("data %s\n", client_message);
		direction = (float)atof(client_message);
		//Send the message back to client
		write(sock, client_message, strlen(client_message));
		//clear the message buffer
		memset(client_message, 0, 5);
	}
	if (read_size == 0) {
		printf("Client disconnected");
		fflush(stdout);
	} else if (read_size == -1) {
		perror("recv failed");
	}
	printf("connectionHandeler pthread succeed.");
	return 0;
}

void* createConnection(void* param) {
	while (1) {
		//Listen
		listen(socket_desc, 3);

		//Accept and incoming connection
		printf("Now waiting for incoming connections");

		pthread_t thread_id;
		d = sizeof(struct sockaddr_in);
		if ((client_sock = accept(socket_desc, (struct sockaddr *) &client,
				(socklen_t*) &d))) {
			printf("Connection accepted");

			if (pthread_create(&thread_id, NULL, connection_handler,
					(void*) &client_sock) < 0) {
				perror("could not create thread");
				return 0;
			}

			//Now join the thread , so that we dont terminate before the thread
			printf("Handler assigned");
		}

		if (client_sock < 0) {
			perror("accept failed");
			return 0;
		}
	}
	return 0;
}

int main() {

	//create a thread for the socket connection
	pthread_t thread_createConnection;
	// create a thread for the motor
	pthread_t thread_motor;
	// create a thread for the compass
	pthread_t thread_compass;
	// creathe a thread for the distacne sensor
	pthread_t thread_distance;

	//initialize the connection settings
	connectionInit();
	//initialize the motor settings
	motorInit();
	//initialize the compass settings
	compassInit();
	//initialize the distance sensor settings
	distanceInit();

	//run the threads
	pthread_create(&thread_createConnection, NULL, createConnection, 0);
	printf("Connection established. \n");
	pthread_create(&thread_motor, NULL, motor, 0);
	printf("Engine started. \n");
	pthread_create(&thread_compass, NULL, compass, 0);
	printf("Compass initialized. \n");
	pthread_create(&thread_distance, NULL, distance, 0);
	printf("Distance sensor initialized. \n");

	pthread_join(thread_createConnection, NULL);
	pthread_join(thread_motor, NULL);
	pthread_join(thread_compass, NULL);
	pthread_join(thread_distance, NULL);

	return 0;
}

