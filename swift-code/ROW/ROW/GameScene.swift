import SpriteKit
import SwiftSocket
import CoreGraphics
import Darwin

class GameScene: SKScene {
    let connectBtn = SKLabelNode()
    let rotateAnalogStick = AnalogJoystick(diameter: 100) // from Class
    let client = TCPClient(address: "rowpi1.ddns.net", port: 8888)
    
    override func didMove(to view: SKView) {
        /* Setup your scene here */
        physicsBody = SKPhysicsBody(edgeLoopFrom: frame)
        rotateAnalogStick.position = CGPoint(x: rotateAnalogStick.radius + 10, y: rotateAnalogStick.radius + 10)
        addChild(rotateAnalogStick)
        
        let selfHeight = frame.height
        let btnsOffset: CGFloat = 10
        let joystickAngleLabel = SKLabelNode()
//        joystickAngleLabel.text = "Angle: \(angle)!"
        joystickAngleLabel.text = "Angle: 0.0"
        joystickAngleLabel.fontSize = 20
        joystickAngleLabel.fontColor = UIColor.red
        joystickAngleLabel.horizontalAlignmentMode = .left
        joystickAngleLabel.verticalAlignmentMode = .top
        joystickAngleLabel.position = CGPoint(x: btnsOffset, y: selfHeight - btnsOffset)
        addChild(joystickAngleLabel)
    
        createConnection()
        setRandomStickColor()
        view.isMultipleTouchEnabled = true
        
        rotateAnalogStick.trackingHandler = { [unowned self] jData in
            //          client.send(string: jData.angular)
            let angle = jData.angular != 0.0 ? CGFloat(180.0 - jData.angular * 180.0 / CGFloat.pi) : 0.0
            let radiale = jData.angular != 0.0 ? CGFloat(jData.angular) : 0.0
//            let s = String(format: "%.3f", Double(angle))
//            let bytes1 = angle.bitPattern
//            self.client.send(string: (String(format: "%.3f", Double(angle))))
            self.client.send(string: (String(format: "%.3f", Double(radiale))))
//            print(description)
            joystickAngleLabel.text = "Angle: \(angle)"
        }
        
        rotateAnalogStick.stopHandler =  { [unowned self] in
            joystickAngleLabel.text = "Angle: 0.0"
        }
    }
    
    func setRandomStickColor() {
        let randomColor = UIColor.random()
        rotateAnalogStick.stick.color = UIColor(red:0.36, green:0.30, blue:0.56, alpha:1.0)
    }
    
    func setRandomSubstrateColor() {
        let randomColor = UIColor.random()
        rotateAnalogStick.substrate.color = UIColor(red:0.81, green:0.79, blue:0.87, alpha:1.0)
    }
    
    override func update(_ currentTime: TimeInterval) {
        /* Called before each frame is rendered */
    }
    
    func createConnection(){
        switch self.client.connect(timeout: 1) {
        case .success:
            print("socket connected")
        case .failure(let error):
            print(error)
        }
    }
}

extension UIColor {
    
    static func random() -> UIColor {
        return UIColor(red: CGFloat(drand48()), green: CGFloat(drand48()), blue: CGFloat(drand48()), alpha: 1)
    }
}
